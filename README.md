# Information / Информация

SPEC-файл для создания RPM-пакета **libssh2**.

## Install / Установка

1. Подключить репозиторий **MARKETPLACE**: `dnf copr enable marketplace/libs`.
2. Установить пакет: `dnf install libssh2`.

## Donation / Пожертвование

- [Donating](https://donating.gitlab.io/)